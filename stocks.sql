CREATE TABLE stocks(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	share_count INTEGER NOT NULL,
	price_dollars INTEGER NOT NULL,
	price_cents INTEGER NOT NULL,
	yield REAL NOT NULL,
	ticker TEXT NOT NULL,
	company_name TEXT NOT NULL
);

