#! /bin/python3

import sqlite3
import os
import requests
import time
import numpy as np

from dotenv import load_dotenv
from tabulate import tabulate
from datetime import date
from matplotlib import pyplot as plt


def add(db, con):

    ticker = input("Input Ticker: ")
    num_shares = input("Input # of shares: ")

    load_dotenv()
    key = os.getenv('KEY')
    url = os.getenv('URL')
    url += "OVERVIEW&symbol={}&apikey={}".format(ticker, key)
    r = requests.get(url)
    data = r.json()
    cmp_name = None
    try:
        cmp_name = data['Name']
    except Exception:
        print("Ticker not found")
        db.close()
        con.close()

    div_yield = data['DividendYield']

    if div_yield is None:
        div_yield = 0.0

    # sleep to not arouse suspicion from alphavantage
    time.sleep(1)

    url = os.getenv('URL')
    url += "GLOBAL_QUOTE&symbol={}&apikey={}".format(ticker, key)
    r = requests.get(url)
    data = r.json()

    price = data['Global Quote']['02. open']
    price_arr = price.split(".")
    dollars = price_arr[0]
    cents = price_arr[1]

    db.execute("INSERT INTO stocks VALUES (?, ?, ?, ?, ?, ?, ?)",
               (None, num_shares, dollars, cents, div_yield, ticker, cmp_name))

    print("Ticker: ", ticker)
    print("# of shares: ", num_shares)
    print("Name: ", cmp_name)
    print("Yield: ", div_yield)
    print("Prices/Share", dollars, ".", cents)

    commit = input("Commit addition? (y)es or (n)o: ")

    if "y" == commit:
        con.commit()


def report(db, con):
    rows = db.execute("SELECT * FROM stocks").fetchall()
    table = []
    total = 0
    total_yield = 0
    tickers = []
    percentages = []

    for row in rows:
        value = round(row[1] * (row[2] + row[3] / 10000), 2)
        table.append([row[5], row[6], row[1], str(row[2]) + "." +
                     str(row[3]), value, round(row[4] * 100, 2)])
        total += value

    for row in table:
        percentage = float(row[4]) / total
        percentages.append(percentage)
        tickers.append(row[0])
        row.append(round(row[4] / total * 100, 2))
        total_yield += (percentage * row[5])

    fig = plt.figure(figsize=(10, 7))
    plt.pie(percentages, labels=tickers)

    table.insert(0, ["SYM", "NAME", "# OF SHARES",
                 "PRICE/SHARE", "TOTAL VALUE", "YIELD", "% OF PORT."])

    cmd = input("Print to (T)erminal - default, (F)ile: ")

    if "F" == cmd or "f" == cmd:
        file_name = input("Input output file name (default is today's date): ")

        if "" == file_name:
            file_name = str(date.today())

        f = open(file_name+".txt", "w")
        f.write(tabulate(table))
        f.write("\n")
        f.write("Portfolio Total: " + str(total))
        f.write("\n")
        f.write("Portfolio Yield: " + str(round(total_yield, 2)))
        f.write("\n")
        f.write("Estimated Dividends: " +
                str(round(total * (total_yield / 100), 2)))
        f.write("\n")
        f.close()
        plt.savefig("./" + file_name + ".pdf")
    else:
        plt.show()
        print(tabulate(table))
        print("Portfolio Total:", total)
        print("Portfolio Yield:", round(total_yield, 2))
        print("Estimated Dividends: " +
              str(round(total * (total_yield / 100), 2)))


def change(db, conn):
    symb = input("Enter symbol to update:")
    rows = db.execute("SELECT * FROM stocks").fetchall()

    for row in rows:
        if symb == row[5]:
            cmd = input("(D)elete, (U)pdate count, Change (P)rice or (C) to cancel: ")
            if "D" == cmd or "d" == cmd:
                db.execute("DELETE FROM stocks WHERE id=?", (row[0], ))
                return
            elif "U" == cmd or "u" == cmd:
                count = input("Enter new count:")
                db.execute(
                    "UPDATE stocks SET share_count=? WHERE id=?", (count, row[0]))
                return
            elif "P" == cmd or "p" == cmd:
                price = input("Enter new price (ex. 32.68): ")
                price_array = price.split(".")
                db.execute(
                    "UPDATE stocks SET price_dollars=?,price_cents=? WHERE id=?",
                    (int(price_array[0]), int(price_array[1]), row[0]))
                return
            elif "C" == cmd or "c" == cmd:
                print("Change aborted.")
                return
            else:
                print("Invalid input, change aborted.")
                return

    print("Ticker not found")

# update data w/ api call


def refresh(db, con):
    rows = db.execute("SELECT * FROM stocks").fetchall()

    for row in rows:
        ticker = row[5]

        load_dotenv()
        key = os.getenv('KEY')
        url = os.getenv('URL')
        url += "OVERVIEW&symbol={}&apikey={}".format(ticker, key)
        r = requests.get(url)
        data = r.json()
        try:
            cmp_name = data['Name']
        except Exception:
            print("***NOTE: Ticker: ",
                  ticker,
                  "Data could not be retrieved, please update manually ***")
            continue

        div_yield = data['DividendYield']

        # sleep to not arouse suspicion from alphavantage
        time.sleep(1)

        url = os.getenv('URL')
        url += "GLOBAL_QUOTE&symbol={}&apikey={}".format(ticker, key)
        r = requests.get(url)
        data = r.json()

        price = data['Global Quote']['02. open']
        price_arr = price.split(".")
        dollars = price_arr[0]
        cents = price_arr[1]

        db.execute("UPDATE stocks SET price_dollars=?,price_cents=?,yield=? WHERE ticker=?",
                   (dollars, cents, div_yield, ticker))
        con.commit()


def main():
    conn = sqlite3.connect("main.db")
    cursor = conn.cursor()

    cmd = ""
    while cmd != "q":
        cmd = input(
            "Enter command: (a)dd, (r)eport, r(e)fresh data, (c)hange, (p)rint, (q)uit: ")
        if "A" == cmd or "a" == cmd:
            add(cursor, conn)
        elif "R" == cmd or "r" == cmd:
            report(cursor, conn)
        elif "C" == cmd or "c" == cmd:
            change(cursor, conn)
        elif "E" == cmd or "e" == cmd:
            refresh(cursor, conn)

    cursor.close()
    conn.close()


main()
